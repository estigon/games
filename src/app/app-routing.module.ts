import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { 
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomeModule)
  },
  { 
    path: 'genres',
    loadChildren: () => import('./pages/genres/genres.module').then( m => m.GenresModule)
  },
  { 
    path: 'platforms',
    loadChildren: () => import('./pages/platforms/platforms.module').then( m => m.PlatformsModule)
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
