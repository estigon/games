import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlatformsComponent } from './platforms/platforms.component';

const routes: Routes = [
  {
    path: '',
    component: PlatformsComponent
    // children: [
    //   { path: '', component:  PlatformsComponent},
    //   { path: '**', redirectTo: 'platforms' }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlatformsRoutingModule { }
