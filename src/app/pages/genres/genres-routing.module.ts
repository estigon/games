import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenresComponent } from './genres/genres.component';

const routes: Routes = [
  {
    path: '',
    component:  GenresComponent
    // children: [
    //   { path: 'genres', component:  GenresComponent},
    //   { path: '**', redirectTo: 'genres' }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenresRoutingModule { }
