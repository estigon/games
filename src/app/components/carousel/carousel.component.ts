import { Component, OnInit } from '@angular/core';

// import Swiper core and required modules
import SwiperCore, { Navigation, Swiper, Autoplay, EffectCoverflow, Pagination } from 'swiper';

// install Swiper modules
SwiperCore.use([Navigation, Autoplay, EffectCoverflow, Pagination]);

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {

  breakpoints = {
    320: { slidesPerView: 1, spaceBetween: 20 },
    640: { slidesPerView: 1, spaceBetween: 20 },
    1024: { slidesPerView: 3, spaceBetween: 50 }
  };

  autoplayConfig = {
    delay: 3500,
    disableOnInteraction: false,
  }

  coverflowEffect = {
    rotate: 20,
    stretch: 0,
    // depth: 100,
    depth: 500,
    modifier: 1,
    slideShadows: true,
  }

  constructor() { }

  ngOnInit(): void {
  }

  onSwiper(swiper: Event) {
    console.log(swiper);
  }
  onSlideChange() {
    console.log('slide change');
  }

}
