import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from '../banner/banner.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { CarouselComponent } from '../carousel/carousel.component';
import { CardComponent } from '../card/card.component';
import { SwiperModule } from 'swiper/angular';

import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    BannerComponent,
    NavbarComponent,
    CarouselComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    SwiperModule,
    RouterModule
  ],
  exports: [
    BannerComponent,
    NavbarComponent,
    CarouselComponent,
    CardComponent
  ]
})
export class SharedModule { }
